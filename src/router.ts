import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

import Dashboard from './views/Dashboard.vue';
import Forms from './views/Forms.vue';
import FormsDetail from './views/FormsDetail.vue';
import Tables from './views/Tables.vue';
import UIElements from './views/UIElements.vue';
import Login from './views/Login.vue';
import Modal from './views/Modal.vue';
import Card from './views/Card.vue';
import Group from './views/Group.vue';
import GroupDetail from './views/GroupDetail.vue';
import Contact from './views/Contact.vue';
import ContactDetail from './views/ContactDetail.vue';
import NotFound from './views/NotFound.vue';
import AddForms from './views/FormsAdd.vue';
import EditForms from './views/FormsEdit.vue';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: { layout: 'empty' },
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
  },
  {
    path: '/forms',
    name: 'Forms',
    component: Forms,
  },
  {
    path: '/forms/:id',
    name: 'FormsDetail',
    component: FormsDetail,
    props: true,
  },
  {
    path: '/forms/add-forms',
    name: 'AddForms',
    component: AddForms,
  },
  {
    path: '/forms/edit-forms/:id',
    name: 'EditForms',
    component: EditForms,
    props: true,
  },
  {
    path: '/cards',
    name: 'Cards',
    component: Card,
  },
  {
    path: '/tables',
    name: 'Tables',
    component: Tables,
  },
  {
    path: '/ui-elements',
    name: 'UIElements',
    component: UIElements,
  },
  {
    path: '/modal',
    name: 'Modal',
    component: Modal,
  },
  {
    path: '/group',
    name: 'Group',
    component: Group,
  },
  {
    path: '/group/:id',
    name: 'GroupDetail',
    component: GroupDetail,
    props: true,
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact,
  },
  {
    path: '/contact/:id',
    name: 'ContactDetail',
    component: ContactDetail,
    props: true,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

export default router;
