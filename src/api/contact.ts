// contactsApi.ts
import axios from 'axios';
import { ref } from 'vue';

interface Contacts {
  _id: string;
  name: string;
  channel: [];
  telephone: string;
  is_valid: boolean;
}

const contacts = ref<Contacts[]>([]);
const contactsDetail = ref<Contacts>({
  _id: '',
  name: '',
  channel: [],
  telephone: '',
  is_valid: false,
});

export const fetchContacts = async () => {
  try {
    const response = await axios.get<{ data: Contacts[] }>(
      'https://backend-interview-bot.vercel.app/api/v1/contacts',
      {
        headers: {
          'Access-Control-Allow-Origin': 'http://localhost:8080',
        },
      }
    );
    return response.data.data;
  } catch (error) {
    console.log(error);
    return [];
  }
};
