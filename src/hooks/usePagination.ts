export function getTotalPages(forms: any, itemsPerPage: number) {
  return Math.ceil(forms.length / itemsPerPage);
}

export function getPages(dataState: any, totalPages: number, maxPages: number) {
  let pages = [];
  let startPage = 1;
  let endPage = totalPages;
  if (totalPages > maxPages) {
    const middlePage = Math.floor(maxPages / 2);
    startPage = Math.max(dataState.currentPage - middlePage, 1);
    endPage = Math.min(startPage + maxPages - 1, totalPages);
    if (endPage - startPage < maxPages) {
      startPage = endPage - maxPages + 1;
    }
  }
  for (let i = startPage; i <= endPage; i++) {
    pages.push(i);
  }
  return pages;
}

export function getDisplayedData(forms: any, dataState: any) {
  let startIndex = (dataState.currentPage - 1) * dataState.itemsPerPage;
  let endIndex = startIndex + dataState.itemsPerPage;
  return forms.slice(startIndex, endIndex);
}
